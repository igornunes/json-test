package com.crossover.trial.properties.reader;

import com.crossover.trial.properties.decorator.TypeDiscover;
import com.crossover.trial.properties.pojo.PropertyCustom;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

/**
 * read the properties file
 * @author igor
 */
public class PropertiesReader {

    /**
     * process the property object
     * @param properties
     * @return set of PropertyCustom
     */
    public Set<PropertyCustom> processFile(Properties properties) {
        Set<PropertyCustom> customs = new HashSet<>();
        for (Iterator<Object> it = properties.keySet().iterator(); it.hasNext();) {
            String key = (String) it.next();
            customs.add(new PropertyCustom(key.toLowerCase(), 
                    TypeDiscover.discoverType(key.toLowerCase(),properties.getProperty(key)), 
                    properties.getProperty(key)));
        }        
        return customs;
    }
    
}
