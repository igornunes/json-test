package com.crossover.trial.properties.pojo;

import java.util.Objects;

/**
 * used for represent output and to define when two properties are equals or not
 * @author igor
 */
public class PropertyCustom implements Comparable<PropertyCustom>{
    
    /**
     * property name
     */
    private String propertyName;
    /**
     * property type
     */
    private String propertyType;
    /**
     * property value
     */
    private String propertyValue;

    /**
     * constructor
     * @param propertyName
     * @param propertyType
     * @param propertyValue 
     */
    public PropertyCustom(String propertyName, String propertyType, String propertyValue) {
        this.propertyName = propertyName;
        this.propertyType = propertyType;
        this.propertyValue = propertyValue;
    }

    /**
     * get property name
     * @return name
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * get property type
     * @return type
     */
    public String getPropertyType() {
        return propertyType;
    }

    /**
     * get property value
     * @return value
     */
    public String getPropertyValue() {
        return propertyValue;
    }

    /** 
     * calculate the hash code of object
     * @return integer
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.propertyName);
        return hash;
    }

    /**
     * test if object is equals
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PropertyCustom other = (PropertyCustom) obj;
        if (!Objects.equals(this.propertyName, other.propertyName)) {
            return false;
        }
        return true;
    }

    /**
     * comparator method
     * @param other
     * @return -1 to minor, 0 to equal or 1 to major
     */
    @Override
    public int compareTo(PropertyCustom other) {
        return this.getPropertyName().compareTo(other.getPropertyName());
    }
    
}
