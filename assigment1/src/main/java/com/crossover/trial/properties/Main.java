package com.crossover.trial.properties;

import com.crossover.trial.properties.pojo.PropertyCustom;
import com.crossover.trial.properties.processor.FileProcessor;
import com.crossover.trial.properties.processor.FileProcessorImpl;
import java.util.Set;

/**
 * assignment1 crossover test
 *
 * @author igor
 */
public class Main {

    /**
     * main method to run the program
     * @param args
     */
    public static void main(String[] args) {
        FileProcessor fileProcessor = new FileProcessorImpl();
        Set<PropertyCustom> customs = fileProcessor.process(args);
        customs.stream().sorted().forEach((custom) -> System.out.println(custom.getPropertyName() + ", "
                + custom.getPropertyType() + ", " + custom.getPropertyValue()));
    }
}
