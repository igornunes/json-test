/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crossover.trial.properties.exception;

/**
 * custom exception that be throws when file type cannot expected
 * @author igor
 */
public class InvalidFileExtensionException extends Exception {

    private static final long serialVersionUID = 7012249065693325869L;

    /**
     * constructor
     */
    public InvalidFileExtensionException() {
        super();
    }

     /**
      * constructor
      * @param message 
      */
    public InvalidFileExtensionException(String message) {
        super(message);
    }

    /**
     * constructor
     * @param message
     * @param cause 
     */
    public InvalidFileExtensionException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor
     * @param cause 
     */
    public InvalidFileExtensionException(Throwable cause) {
        super(cause);
    }
}
