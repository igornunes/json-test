package com.crossover.trial.properties.loader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;

/**
 * get the file
 * @author igor
 */
public interface FileLoader {
    
    /**
     * method responsible for reading files 
     * @param source
     * @return Reader
     * @throws FileNotFoundException
     * @throws IOException 
     */
    Reader loadFile(String source) throws FileNotFoundException, IOException;
    
}
