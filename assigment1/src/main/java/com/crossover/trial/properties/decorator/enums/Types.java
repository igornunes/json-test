package com.crossover.trial.properties.decorator.enums;

/**
 * define specific types 
 * @author igor
 */
public enum Types {
    /**
     * specific type for amazon Regions key
     */
    aws_region_id("com.amazonaws.regions.Regions"),
    /**
     * specific type for amazon ID key
     */
    aws_account_id("java.lang.Integer");

    private final String type;

    /**
     * constructor
     * @param s 
     */
    Types(String s) { 
        this.type = s; 
    }

    /**
     * get the type of specific key
     * @return type
     */
    public String getValue() { return type; }

}
