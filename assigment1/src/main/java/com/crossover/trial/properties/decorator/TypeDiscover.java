package com.crossover.trial.properties.decorator;

import com.crossover.trial.properties.decorator.enums.Types;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * get property value and try check in enumerator and convert for specific types
 * (integer, boolean, etc). return the type of property
 *
 * @author igor
 */
public class TypeDiscover {

    /**
     * method to discover the object type of key
     * @param key
     * @param value
     * @return type
     */
    public static String discoverType(String key, String value) {
        List enumValues = Arrays.asList(Types.values());
        String type = null;
        if (key != null) {
            for (Object enumValue : enumValues) {
                String aux = enumValue.toString();
                if (aux.equals(key.toLowerCase())) {
                    return ((Types) enumValue).getValue();
                }
            }
        }
        if (value != null) {
            type = convertType(value);
        }        
        if (type == null) {
            type = "java.lang.String";
        }
        return type;
    }

    /**
     * method that use parse to discover types
     * @param value
     * @return type
     */
    private static String convertType(String value) {
        String type = null;
        try {
            Integer.parseInt(value);
            type = "java.lang.Integer";
        } catch (Exception e1) {
            try {
                if (value.toLowerCase().equals("true") || value.toLowerCase().equals("false")) {
                    Boolean.parseBoolean(value);
                    type = "java.lang.Boolean";
                } else {
                    throw new ParseException(null, 0);
                }
            } catch (Exception e2) {
                try {
                    LocalDate.parse(value);
                    type = "java.time.LocalDate";
                } catch (Exception e3) {
                    Logger.getLogger(TypeDiscover.class.getName()).log(Level.SEVERE, null, e3);
                }
            }
        }
        return type;
    }

}
