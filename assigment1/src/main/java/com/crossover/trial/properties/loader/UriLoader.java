package com.crossover.trial.properties.loader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

/**
 * get file from system
 * @author igor
 */
public class UriLoader implements FileLoader {

    /**
     * read file from system path
     * @param source
     * @return Reader
     * @throws FileNotFoundException
     * @throws IOException 
     */
    @Override
    public Reader loadFile(String source) throws FileNotFoundException, IOException{
        FileInputStream is = new FileInputStream(source);
        Reader reader = new BufferedReader(new InputStreamReader(is, Charset.defaultCharset()));
        return reader;
    }
    
}
