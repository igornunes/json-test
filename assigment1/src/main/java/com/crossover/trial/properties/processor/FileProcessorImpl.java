package com.crossover.trial.properties.processor;

import com.crossover.trial.properties.exception.InvalidFileExtensionException;
import com.crossover.trial.properties.loader.ClasspathLoader;
import com.crossover.trial.properties.loader.UriLoader;
import com.crossover.trial.properties.loader.UrlLoader;
import com.crossover.trial.properties.pojo.PropertyCustom;
import com.crossover.trial.properties.reader.JsonReader;
import com.crossover.trial.properties.reader.PropertiesReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 * Process the file list
 *
 * @author igor
 */
public class FileProcessorImpl implements FileProcessor {

    private static final String HTTP_PREFIX = "http";
    private static final String CLASSPATH_PREFIX = "classpath:";
    private Set<PropertyCustom> finalSet = new HashSet<>();

    /**
     * main method to process the file list
     *
     * @param sources
     * @return set of PropertyCustom
     */
    @Override
    public Set<PropertyCustom> process(String[] sources) {
        Reader reader;
        try {
            for (String source : sources) {
                Set<PropertyCustom> customs = new HashSet<>();
                reader = loadFile(source);
                customs.addAll(readFile(reader, source));
                updateFinalSet(customs);
            }
        } catch (IOException ex) {
            Logger.getLogger(FileProcessorImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return finalSet;
    }

    /**
     * method that load the file
     *
     * @param source
     * @return Reader
     * @throws IOException
     */
    private Reader loadFile(String source) throws IOException {
        Reader reader;
        if (source.toLowerCase().startsWith(HTTP_PREFIX)) {
            reader = new UrlLoader().loadFile(source);
        } else if (source.toLowerCase().startsWith(CLASSPATH_PREFIX)) {
            reader = new ClasspathLoader().loadFile(source);
        } else {
            reader = new UriLoader().loadFile(source);
        }
        return reader;
    }

    /**
     * method that read the file
     *
     * @param reader
     * @return Set<PropertyCustom>
     */
    private Set<PropertyCustom> readFile(Reader reader, String source) throws IOException {
        Set<PropertyCustom> customs = new HashSet<>();
        if (source.toLowerCase().endsWith(".properties")) {
            Properties prop = new Properties();
            prop.load(reader);
            customs.addAll(new PropertiesReader().processFile(prop));
        } else if (source.toLowerCase().endsWith(".json")) {
            String jsonText = readAll(reader);
            JSONObject json = new JSONObject(jsonText);
            customs.addAll(new JsonReader().processFile(json));
        } else {
            try {
                throw new InvalidFileExtensionException();
            } catch (InvalidFileExtensionException ex) {
                Logger.getLogger(FileProcessorImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return customs;
    }

    /**
     * auxiliary method to read entire file and create a string to be used in
     * json object constructor
     *
     * @param reader
     * @return String
     * @throws IOException
     */
    public static String readAll(Reader reader) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = reader.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    private void updateFinalSet(Set<PropertyCustom> customs) {
        for (PropertyCustom custom : customs) {
            if (finalSet.contains(custom)) {
                finalSet.remove(custom);
                finalSet.add(custom);
            } else {
                finalSet.add(custom);
            }
        }
    }
}
