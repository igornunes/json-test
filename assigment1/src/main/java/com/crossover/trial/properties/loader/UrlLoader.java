package com.crossover.trial.properties.loader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

/**
 *
 * @author igor
 */
public class UrlLoader implements FileLoader {

    /**
     * read file from URL
     * @param source
     * @return Reader
     * @throws FileNotFoundException
     * @throws IOException 
     */
    @Override
    public Reader loadFile(String source) throws FileNotFoundException, IOException {
        Reader reader = null;
        URL url = new URL(source);
        URLConnection urlConn = url.openConnection();
        if (urlConn != null) {
            urlConn.setReadTimeout(60 * 1000);
        }
        if (urlConn.getInputStream() != null) {
            InputStreamReader in = new InputStreamReader(urlConn.getInputStream(),
                    Charset.defaultCharset());
            reader = new BufferedReader(in);
        }
        return reader;
    }
}
