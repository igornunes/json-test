package com.crossover.trial.properties.loader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * load files from classpath
 *
 * @author igor
 */
public class ClasspathLoader implements FileLoader {

    /**
     * read the file from path in classpath
     * @param source
     * @return Reader
     * @throws FileNotFoundException
     * @throws IOException
     */
    @Override
    public Reader loadFile(String source) throws FileNotFoundException, IOException {
        if (source.contains("/")) {
            source = source.replace(source.substring(0,source.lastIndexOf("/")+1),"");
        } else {
            source = source.toLowerCase().replace("classpath:","");
        }
        return (Reader) new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(source)));
    }

}
