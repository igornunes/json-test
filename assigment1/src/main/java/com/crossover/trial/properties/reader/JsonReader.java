package com.crossover.trial.properties.reader;

import com.crossover.trial.properties.decorator.TypeDiscover;
import com.crossover.trial.properties.pojo.PropertyCustom;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONObject;

/**
 * read the json file
 * @author igor
 */
public class JsonReader {

    /**
     * process json object
     * @param json
     * @return Set of PropertyCustom
     */
    public Set<PropertyCustom> processFile(JSONObject json) {
        Set<PropertyCustom> customs = new HashSet<>();
        String[] keys = JSONObject.getNames(json);
        for (String key : keys) {
            String value = json.get(key).toString();
            customs.add(new PropertyCustom(key.toLowerCase(),
                    TypeDiscover.discoverType(key.toLowerCase(), value), value));
        }
        return customs;
    }
}
