package com.crossover.trial.properties.processor;

import com.crossover.trial.properties.pojo.PropertyCustom;
import java.util.Set;

/**
 * interface to process property files
 * @author igor
 */
public interface FileProcessor {
    
    /**
     * process a list of files
     * @param sources
     * @return List of PropertyCustom
     */
    Set<PropertyCustom> process(String[] sources);
    
}
