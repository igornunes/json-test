package com.crossover.trial.properties.test;

import com.crossover.trial.properties.loader.ClasspathLoader;
import com.crossover.trial.properties.pojo.PropertyCustom;
import com.crossover.trial.properties.processor.FileProcessorImpl;
import com.crossover.trial.properties.reader.JsonReader;
import com.crossover.trial.properties.reader.PropertiesReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.Assert;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

/**
 * reader test
 *
 * @author igor
 */
public class ReaderTest {

    private static final String PROPERTY = "classpath:resources/jdbc.properties";
    private static final String JSON = "classpath:resources/config.json";
    private static final String TEXT = "classpath:resources/textfile.txt";

    private Reader propReader;
    private Reader jsonReader;
    private Reader txtReader;

    /**
     * prepare tests
     */
    @Before
    public void init() {
        ClasspathLoader loader = new ClasspathLoader();
        try {
            propReader = loader.loadFile(PROPERTY);
            jsonReader = loader.loadFile(JSON);
            txtReader = loader.loadFile(TEXT);
        } catch (IOException ex) {
            Logger.getLogger(LoaderTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * test properties file
     */
    @Test
    public void propertyReaderTest() {
        Set<PropertyCustom> customs = new HashSet<>();
        Properties prop = new Properties();
        try {
            prop.load(propReader);
            customs.addAll(new PropertiesReader().processFile(prop));
        } catch (IOException ex) {
            Logger.getLogger(LoaderTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        Assert.assertEquals(7, customs.size());
    }

    /**
     * test json file
     */
    @Test
    public void jsonReaderTest() {
        Set<PropertyCustom> customs = new HashSet<>();
        String jsonText;
        JSONObject json = null;
        try {
            jsonText = FileProcessorImpl.readAll(jsonReader);
            json = new JSONObject(jsonText);
        } catch (IOException ex) {
            Logger.getLogger(ReaderTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        customs.addAll(new JsonReader().processFile(json));
        Assert.assertEquals(7, customs.size());
    }

    /**
     * test wrong content file
     */
    @Test
    public void txtReaderTest() {
        Properties prop = new Properties();
        JSONObject json = null;
        try {
            prop.load(txtReader);
            json = new JSONObject(FileProcessorImpl.readAll(txtReader));
        } catch (Exception ex) {
            Logger.getLogger(LoaderTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Assert.assertNull(prop.getProperty(prop.elements().nextElement().toString()));
            Assert.assertNull(json);
        }
    }

}
