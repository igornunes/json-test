package com.crossover.trial.properties.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * suite test
 * @author igor
 */
@RunWith(Suite.class)
@SuiteClasses({ LoaderTest.class, ReaderTest.class, TypeDiscoverTest.class, ProcessTest.class })
public class AllTests {
    
}
