package com.crossover.trial.properties.test;

import com.crossover.trial.properties.loader.ClasspathLoader;
import com.crossover.trial.properties.loader.UriLoader;
import com.crossover.trial.properties.loader.UrlLoader;
import java.io.IOException;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.Assert;
import org.junit.Test;

/**
 * test the loader
 *
 * @author igor
 */
public class LoaderTest {

    private static final String URL = "https://bitbucket.org/igornunes/json-test/raw/3db65d176b256b201793231148bc5d665b51d5c4/test.json";
    private static final String URL_ERROR = "http://www.google.com.asdf";
    private static final String SYSTEM_PATH = "/tmp/aws.properties";
    private static final String SYSTEM_PATH_ERROR = "/tmpS/aws.propertiesss";
    private static final String CLASSPATH = "classpath:resources/jdbc.properties";
    private static final String CLASSPATH_ERROR = "classpath:resourcesss/jdbcsss.propertiesss";

    /**
     * test the url loader
     */
    @Test
    public void UrlLoaderTest() {
        UrlLoader loader = new UrlLoader();
        Reader reader = null;
        try {
            reader = loader.loadFile(URL);
        } catch (IOException ex) {
            Logger.getLogger(LoaderTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        Assert.assertNotNull(reader);
    }

    /**
     * test the url loader
     */
    @Test
    public void UrlLoaderErrorTest() {
        UrlLoader loader = new UrlLoader();
        Reader reader = null;
        try {
            reader = loader.loadFile(URL_ERROR);
        } catch (Exception ex) {
            Logger.getLogger(LoaderTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Assert.assertNull(reader);
        }
    }

    /**
     * test the classpath loader
     */
    @Test
    public void ClasspathLoaderTest() {
        ClasspathLoader loader = new ClasspathLoader();
        Reader reader = null;
        try {
            reader = loader.loadFile(CLASSPATH);
        } catch (IOException ex) {
            Logger.getLogger(LoaderTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        Assert.assertNotNull(reader);
    }

    /**
     * test the classpath loader
     */
    @Test
    public void ClasspathLoaderErrorTest() {
        ClasspathLoader loader = new ClasspathLoader();
        Reader reader = null;
        try {
            reader = loader.loadFile(CLASSPATH_ERROR);
        } catch (Exception ex) {
            Logger.getLogger(LoaderTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Assert.assertNull(reader);
        }
    }

    /**
     * test the uri loader
     */
    @Test
    public void UriLoaderTest() {
        UriLoader loader = new UriLoader();
        Reader reader = null;
        try {
            reader = loader.loadFile(SYSTEM_PATH);
        } catch (IOException ex) {
            Logger.getLogger(LoaderTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        Assert.assertNotNull(reader);
    }

    /**
     * test the uri loader
     */
    @Test
    public void UriLoaderErrorTest() {
        UriLoader loader = new UriLoader();
        Reader reader = null;
        try {
            reader = loader.loadFile(SYSTEM_PATH_ERROR);
        } catch (Exception ex) {
            Logger.getLogger(LoaderTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Assert.assertNull(reader);
        }
    }

}
