package com.crossover.trial.properties.test;

import com.crossover.trial.properties.pojo.PropertyCustom;
import com.crossover.trial.properties.processor.FileProcessor;
import com.crossover.trial.properties.processor.FileProcessorImpl;
import java.util.Set;
import org.junit.Test;

/**
 * processor test
 * @author igor
 */
public class ProcessTest {
    
    @Test
    public void runProcessorTest(){
        String[] args = new String[4];
        args[0] = "https://bitbucket.org/igornunes/json-test/raw/3db65d176b256b201793231148bc5d665b51d5c4/test.json";
        args[1] = "/tmp/aws.properties";
        args[2] = "classpath:resources/jdbc.properties";
        args[3] = "classpath:config.json";
        FileProcessor fileProcessor = new FileProcessorImpl();
        Set<PropertyCustom> customs = fileProcessor.process(args);
        customs.stream().sorted().forEach((custom) -> System.out.println(custom.getPropertyName() + ", "
                + custom.getPropertyType() + ", " + custom.getPropertyValue()));
    }
    
}
