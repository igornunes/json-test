package com.crossover.trial.properties.test;

import com.crossover.trial.properties.decorator.TypeDiscover;
import junit.framework.Assert;
import org.junit.Test;

/**
 * type discover test
 * @author igor
 */
public class TypeDiscoverTest {

    /**
     * test enumerator type
     */
    @Test
    public void testDiscoverEnum1(){
        Assert.assertEquals("java.lang.Integer",TypeDiscover.discoverType("aws_account_id", "12345678"));        
    }
    
    /**
     * test enumerator type
     */
    @Test
    public void testDiscoverEnum2(){
        Assert.assertEquals("com.amazonaws.regions.Regions",TypeDiscover.discoverType("aws_region_id", "us-east-1"));        
    }
    
    /**
     * test boolean type
     */
    @Test
    public void testDiscoverBoolean(){
        Assert.assertEquals("java.lang.Boolean",TypeDiscover.discoverType("jpa.showSql", "true"));        
    }
    
    /**
     * test string type
     */
    @Test
    public void testDiscoverString(){
        Assert.assertEquals("java.lang.String",TypeDiscover.discoverType("JDBC_USERNAME", "username123"));        
    }
    
    /**
     * test integer type
     */
    @Test
    public void testDiscoverInteger(){
        Assert.assertEquals("java.lang.Integer",TypeDiscover.discoverType(null, "123"));        
    }
    
    /**
     * test date type
     */
    @Test
    public void testDiscoverDate(){
        Assert.assertEquals("java.time.LocalDate",TypeDiscover.discoverType(null, "2015-11-11"));        
    }
    
}
