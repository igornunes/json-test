package com.crossover.trial.weather;

import com.crossover.trial.weather.client.WeatherClient;
import com.crossover.trial.weather.model.DataPoint;
import com.crossover.trial.weather.server.WeatherServer;
import com.crossover.trial.weather.service.impl.RestWeatherQueryEndpoint;
import com.crossover.trial.weather.service.impl.RestWeatherCollectorEndpoint;
import com.crossover.trial.weather.service.WeatherCollector;
import com.crossover.trial.weather.service.WeatherQuery;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.junit.Before;
import org.junit.Test;

import java.util.Properties;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

/**
 * test class
 *
 * @author igor
 */
public class WeatherEndpointTest {

    /**
     * query
     */
    private WebTarget query;

    /**
     * update
     */
    private WebTarget update;

    /**
     * gson
     */
    private final Gson gson = new Gson();

    /**
     * dataPoint
     */
    private DataPoint dataPoint;
    
    /**
     * collector
     */
    private WeatherCollector collector;
    
    /**
     * queryAux
     */
    private WeatherQuery queryAux;

    /**
     * pre-execute test
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        WeatherServer.main(null);
        Client client = ClientBuilder.newClient();
        Properties prop = new Properties();
        prop.load(WeatherClient.class.getClassLoader().getResourceAsStream("application.properties"));
        String BASE_URL = prop.getProperty("baseUrl");
        query = client.target(BASE_URL + "query");
        update = client.target(BASE_URL + "collect");
        collector = new RestWeatherCollectorEndpoint();
        queryAux = new RestWeatherQueryEndpoint();
        dataPoint = new DataPoint(10, 22, 20, 30, 10);
    }

    /**
     * test ping
     *
     * @throws Exception
     */
    @Test
    public void testPing() throws Exception {
        WebTarget path = update.path("/ping");
        Response response = path.request().get();
        assertEquals(200, response.getStatus());
    }

    /**
     * test get
     *
     * @throws Exception
     */
    @Test
    public void testGet() throws Exception {
        WebTarget path = query.path("/weather/BOS/0");
        Response response = path.request().get();        
        assertEquals(200, response.getStatus());
    }

    /**
     * test get nearby
     *
     * @throws Exception
     */
    @Test
    public void testGetNearby() throws Exception {
        WebTarget path = update.path("/weather/JFK/wind");
        path.request().post(Entity.json(dataPoint));
        Response response = path.request().get();   
        dataPoint.setMean(40);
        path = update.path("/weather/EWR/wind");
        path.request().post(Entity.json(dataPoint));
        response = path.request().get();  
        path = update.path("/weather/LGA/wind");
        path.request().post(Entity.json(dataPoint));
        response = path.request().get();
        dataPoint.setMean(30);

        WebTarget path2 = query.path("/weather/JFK/200");
        Response response2 = path2.request().get();  
        assertEquals(200, response2.getStatus());
    }

    /**
     * test update
     *
     * @throws Exception
     */
    @Test
    public void testUpdate() throws Exception {

        DataPoint windDp = new DataPoint(10, 22, 20, 30, 10);
        WebTarget path = update.path("/weather/BOS/wind");
        path.request().post(Entity.json(windDp));
        Response response = path.request().get();
        
        WebTarget path2 = query.path("/weather/BOS/0");
        Response response2 = path2.request().get(); 
        
        path2 = query.path("/ping");
        response2 =  path2.request().get();
        
        JsonElement pingResult = new JsonParser().parse(response2.getEntity().toString());
        assertEquals(200, response2.getStatus());
    }

}
