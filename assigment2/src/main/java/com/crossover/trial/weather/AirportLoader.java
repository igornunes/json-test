// CR:restructure package
package com.crossover.trial.weather;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.io.*;

/**
 * A simple airport loader which reads a file from disk and sends entries to the webservice
 *
 * TODO: Implement the Airport Loader
 * 
 * @author code test administrator
 */
// CR:move this to load data after server start
public class AirportLoader {

    /** end point for read queries */
    // CR:not used- remove
    private WebTarget query;

    /** end point to supply updates */
    // CR:not used- remove
    private WebTarget collect;

    // CR:javadoc
    public AirportLoader() {
        Client client = ClientBuilder.newClient();
        // CR:not used- remove
        query = client.target("http://localhost:8080/query");
        // CR:not used- remove
        collect = client.target("http://localhost:8080/collect");
    }

    // CR:javadoc
    public void upload(InputStream airportDataStream) throws IOException{
        // CR:define charset
        BufferedReader reader = new BufferedReader(new InputStreamReader(airportDataStream));
        // CR:wrong logic - remove
        String l = null;
        while ((l = reader.readLine()) != null) {
            break;
        }
    }

    // CR:javadoc
    public static void main(String args[]) throws IOException{
        File airportDataFile = new File(args[0]);
        if (!airportDataFile.exists() || airportDataFile.length() == 0) {
            System.err.println(airportDataFile + " is not a valid input");
            System.exit(1);
        }

        AirportLoader al = new AirportLoader();
        al.upload(new FileInputStream(airportDataFile));
        System.exit(0);
    }
}
