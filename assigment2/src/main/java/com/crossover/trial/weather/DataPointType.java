// CR:restructure package
package com.crossover.trial.weather;

/**
 * The various types of data points we can collect.
 *
 * @author code test administrator
 */
public enum DataPointType {
    // CR:javadoc for all elements
    WIND,
    TEMPERATURE,
    HUMIDTY,
    PRESSURE,
    CLOUDCOVER,
    PRECIPITATION
}
