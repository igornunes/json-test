// CR:restructure package
package com.crossover.trial.weather;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Basic airport information.
 *
 * @author code test administrator
 */
// CR: implement hashcode method
public class AirportData {

    /** the three letter IATA code */
    String iata;

    /** latitude value in degrees */
    double latitude;

    /** longitude value in degrees */
    double longitude;

    // CR:javadoc
    public AirportData() { }

    // CR:javadoc
    public String getIata() {
        return iata;
    }

    // CR:javadoc
    public void setIata(String iata) {
        this.iata = iata;
    }

    // CR:javadoc and use this
    public double getLatitude() {
        return latitude;
    }

    // CR:javadoc
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    // CR:javadoc and use this
    public double getLongitude() {
        return longitude;
    }

    // CR:javadoc
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    // CR:javadoc
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

    // CR: javadoc and override anotation
    public boolean equals(Object other) {
        if (other instanceof AirportData) {
            return ((AirportData)other).getIata().equals(this.getIata());
        }

        return false;
    }
}
