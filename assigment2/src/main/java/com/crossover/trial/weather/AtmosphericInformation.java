// CR:restructure package
package com.crossover.trial.weather;

/**
 * encapsulates sensor information for a particular location
 */
// CR:implement equahs and hashcode
class AtmosphericInformation {

    /** temperature in degrees celsius */
    private DataPoint temperature;

    /** wind speed in km/h */
    private DataPoint wind;

    /** humidity in percent */
    private DataPoint humidity;

    /** precipitation in cm */
    private DataPoint precipitation;

    /** pressure in mmHg */
    private DataPoint pressure;

    /** cloud cover percent from 0 - 100 (integer) */
    private DataPoint cloudCover;

    /** the last time this data was updated, in milliseconds since UTC epoch */
    private long lastUpdateTime;

    // CR:javadoc
    public AtmosphericInformation() {

    }

    // CR:javadoc
    protected AtmosphericInformation(DataPoint temperature, DataPoint wind, DataPoint humidity, DataPoint percipitation, DataPoint pressure, DataPoint cloudCover) {
        this.temperature = temperature;
        this.wind = wind;
        this.humidity = humidity;
        this.precipitation = percipitation;
        this.pressure = pressure;
        this.cloudCover = cloudCover;
        this.lastUpdateTime = System.currentTimeMillis();
    }

     // CR:javadoc , use this
    public DataPoint getTemperature() {
        return temperature;
    }
    
     // CR:javadoc
    public void setTemperature(DataPoint temperature) {
        this.temperature = temperature;
    }
    
     // CR:javadoc, use this
    public DataPoint getWind() {
        return wind;
    }
    
     // CR:javadoc
    public void setWind(DataPoint wind) {
        this.wind = wind;
    }
    
     // CR:javadoc, use this
    public DataPoint getHumidity() {
        return humidity;
    }
    
     // CR:javadoc
    public void setHumidity(DataPoint humidity) {
        this.humidity = humidity;
    }
    
     // CR:javadoc, use this
    public DataPoint getPrecipitation() {
        return precipitation;
    }
     // CR:javadoc
    public void setPrecipitation(DataPoint precipitation) {
        this.precipitation = precipitation;
    }
    
     // CR:javadoc, use this
    public DataPoint getPressure() {
        return pressure;
    }
    
     // CR:javadoc
    public void setPressure(DataPoint pressure) {
        this.pressure = pressure;
    }
    
     // CR:javadoc, use this
    public DataPoint getCloudCover() {
        return cloudCover;
    }
    
     // CR:javadoc
    public void setCloudCover(DataPoint cloudCover) {
        this.cloudCover = cloudCover;
    }
    
     // CR:javadoc
    protected long getLastUpdateTime() {
        return this.lastUpdateTime;
    }
     // CR:javadoc
    protected void setLastUpdateTime(long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }
}
