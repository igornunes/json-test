// CR:restructure package
package com.crossover.trial.weather;

/**
 * An internal exception marker
 */
// CR:implements constructors, and add serialVersionUID
public class WeatherException extends Exception {
}
