package com.crossover.trial.weather.model;

import java.util.Objects;

/**
 * Basic airport information.
 *
 * @author code test administrator
 */
public class AirportData implements Comparable<AirportData> {

    /**
     * id
     */
    private Integer id;
    
    /**
     * city
     */
    private String city;
    
    /**
     * country
     */
    private String country;
    
    
    /**
     * the three letter IATA code
     */
    private String iata;
    
    /**
     * icao
     */
    private String icao;

    /**
     * latitude value in degrees
     */
    private double latitude;

    /**
     * longitude value in degrees
     */
    private double longitude;
    
    /**
     * altitude in feet
     */
    private double altitude;
    
    /**
     * hours offset
     */
    private double timezone;
    
    /**
     * dst
     */
    private String dst;

    /**
     * constructor
     */
    public AirportData() {
    }

    /**
     * constructor
     * @param id
     * @param city
     * @param country
     * @param iata
     * @param icao
     * @param latitude
     * @param longitude
     * @param altitude
     * @param timezone
     * @param dst 
     */
    public AirportData(Integer id, String city, String country, String iata, String icao, double latitude, double longitude, double altitude, double timezone, String dst) {
        this.id = id;
        this.city = city;
        this.country = country;
        this.iata = iata;
        this.icao = icao;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.timezone = timezone;
        this.dst = dst;
    }

    /**
     * getter iata
     *
     * @return iata
     */
    public String getIata() {
        return this.iata;
    }

    /**
     * setter iata
     *
     * @param iata
     */
    public void setIata(String iata) {
        this.iata = iata;
    }

    /**
     * getter latitude
     *
     * @return latitude
     */
    public double getLatitude() {
        return this.latitude;
    }

    /**
     * getter latitude
     *
     * @param latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * getter longitude
     *
     * @return longitude
     */
    public double getLongitude() {
        return this.longitude;
    }

    /**
     * setter longitude
     *
     * @param longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * toString
     *
     * @return String
     */
    @Override
    public String toString() {
        return id + "," + city + "," + country.substring(0,country.indexOf("-")-1) + ","+ 
                country.substring(country.indexOf("-")+2)+ ","+ iata + "," + icao + "," + latitude 
                + "," + longitude + "," + altitude + "," + timezone + "," + dst ;
    }

    /**
     * hashcode
     *
     * @return integer
     */
    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    /**
     * equals
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        boolean equals = true;
        if (obj == null) {
            equals = false;
        }
        if (getClass() != obj.getClass()) {
            equals = false;
        }
        final AirportData other = (AirportData) obj;
        if (!Objects.equals(this.iata, other.iata)) {
            equals = false;
        }
        return equals;
    }

    /**
     * comparator method
     * @param other
     * @return -1 to minor, 0 to equal or 1 to major
     */
    @Override
    public int compareTo(AirportData other) {
        return this.getIata().compareTo(other.getIata());
    }

    /**
     * getter id
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * setter id
     * @param id 
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * getter city
     * @return city
     */
    public String getCity() {
        return city;
    }

    /**
     * setter city
     * @param city 
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * getter country
     * @return country
     */
    public String getCountry() {
        return country;
    }

    /**
     * setter country
     * @param country 
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * getter icao
     * @return icao
     */
    public String getIcao() {
        return icao;
    }

    /**
     * setter icao
     * @param icao 
     */
    public void setIcao(String icao) {
        this.icao = icao;
    }

    /**
     * getter altitude
     * @return altitude
     */
    public double getAltitude() {
        return altitude;
    }

    /**
     * setter altitude
     * @param altitude 
     */
    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    /**
     * getter timezone
     * @return timezone
     */
    public double getTimezone() {
        return timezone;
    }

    /**
     * setter timezone
     * @param timezone 
     */
    public void setTimezone(double timezone) {
        this.timezone = timezone;
    }

    /**
     * getter dst
     * @return dst
     */
    public String getDst() {
        return dst;
    }

    /**
     * setter dst
     * @param dst 
     */
    public void setDst(String dst) {
        this.dst = dst;
    }

}
