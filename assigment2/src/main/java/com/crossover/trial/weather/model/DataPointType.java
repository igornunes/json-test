package com.crossover.trial.weather.model;

/**
 * The various types of data points we can collect.
 *
 * @author code test administrator
 */
public enum DataPointType {

    /**
     * WIND
     */
    WIND("wind"),
    /**
     * TEMPERATURE
     */
    TEMPERATURE("temperature"),
    /**
     * HUMIDTY
     */
    HUMIDITY("humidity"),
    /**
     * PRESSURE
     */
    PRESSURE("pressure"),
    /**
     * CLOUDCOVER
     */
    CLOUDCOVER("cloudover"),
    /**
     * PRECIPITATION
     */
    PRECIPITATION("precipitation");
    
    /**
     * name
     */
    private final String name;
    
    /**
     * constructor
     * @param s 
     */
    DataPointType(String name) { 
        this.name = name; 
    }

    /**
     * get the type of specific key
     * @return type
     */
    public String getValue() { return name; }
}
