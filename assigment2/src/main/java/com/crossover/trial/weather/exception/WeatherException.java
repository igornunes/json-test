package com.crossover.trial.weather.exception;

/**
 * An internal exception marker
 */
public class WeatherException extends Exception {

    private static final long serialVersionUID = -1763593411470554908L;

    /**
     * constructor
     */
    public WeatherException() {
        super();
    }

    /**
     * constructor
     *
     * @param message
     */
    public WeatherException(String message) {
        super(message);
    }

    /**
     * constructor
     *
     * @param message
     * @param cause
     */
    public WeatherException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * constructor
     *
     * @param cause
     */
    public WeatherException(Throwable cause) {
        super(cause);
    }
}
