// CR:restructure package
package com.crossover.trial.weather.server;

import com.crossover.trial.weather.repository.AirportRepository;
import com.crossover.trial.weather.service.impl.RestWeatherCollectorEndpoint;
import com.crossover.trial.weather.service.impl.RestWeatherQueryEndpoint;
import org.glassfish.grizzly.Connection;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.HttpServerFilter;
import org.glassfish.grizzly.http.server.HttpServerProbe;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.String.*;
import java.util.Properties;


/**
 * A main method used to test the Weather Application locally -- live deployment is to a tomcat container.
 *
 * @author code test administrator
 */
public class WeatherServer {
    
    public final static Logger LOGGER = Logger.getLogger(WeatherServer.class.getName());

    private static String BASE_URL;

    /**
     * main method to run server
     * @param args 
     */
    public static void main(String[] args) {
        try {
            Properties prop = new Properties();
            prop.load(WeatherServer.class.getClassLoader().getResourceAsStream("application.properties"));
            BASE_URL = prop.getProperty("baseUrl");
            System.out.println("Starting Weather App local testing server: " + BASE_URL);
            System.out.println("Not for production use");

            final ResourceConfig resourceConfig = new ResourceConfig();
            resourceConfig.register(RestWeatherCollectorEndpoint.class);
            resourceConfig.register(RestWeatherQueryEndpoint.class);
            final HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URL), resourceConfig, false);

            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    AirportRepository.write();
                    server.shutdownNow();
                }
            }));

            HttpServerProbe probe = new HttpServerProbe.Adapter() {
                @Override
                public void onRequestReceiveEvent(HttpServerFilter filter, Connection connection, Request request) {
                    System.out.println(request.getRequestURI());
                }
            };

            server.getServerConfiguration().getMonitoringConfig().getWebServerConfig().addProbes(probe);            
            server.start();
            LOGGER.log(Level.INFO,format("Weather Server started.%n url=%s%n", BASE_URL));
            //used only to load data
            new AirportRepository();
           
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

    }
}
