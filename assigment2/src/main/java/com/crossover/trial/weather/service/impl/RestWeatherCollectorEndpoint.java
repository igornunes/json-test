package com.crossover.trial.weather.service.impl;

import com.crossover.trial.weather.model.AirportData;
import com.crossover.trial.weather.model.AtmosphericInformation;
import com.crossover.trial.weather.model.DataPoint;
import com.crossover.trial.weather.model.DataPointType;
import com.crossover.trial.weather.service.WeatherCollector;
import com.crossover.trial.weather.exception.WeatherException;
import com.crossover.trial.weather.repository.AirportRepository;
import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import static com.crossover.trial.weather.service.impl.RestWeatherQueryEndpoint.*;
import java.util.logging.Level;

/**
 * A REST implementation of the WeatherCollector API. Accessible only to airport
 * weather collection sites via secure VPN.
 *
 * @author code test administrator
 */
@Path("/collect")
public class RestWeatherCollectorEndpoint implements WeatherCollector {

    public final static Logger LOGGER = Logger.getLogger(RestWeatherCollectorEndpoint.class.getName());

    /**
     * shared gson json to object factory
     */
    public final static Gson gson = new Gson();

    static {
        init();
    }

    /**
     * ping method
     *
     * @return status
     */
    @GET
    @Path("/ping")
    @Override
    public Response ping() {
        LOGGER.log(Level.FINE, "ping method");
        return Response.status(Response.Status.OK).entity("ready").build();
    }

    /**
     * update weather
     *
     * @param iataCode
     * @param pointType
     * @param datapointJson
     * @return status
     */
    @POST
    @Path("/weather/{iata}/{pointType}")
    @Override
    public Response updateWeather(@PathParam("iata") String iataCode,
            @PathParam("pointType") String pointType,
            String datapointJson) {
        try {
            LOGGER.log(Level.FINE, "updateWeather -> iata:" + iataCode
                    + " pointType:" + pointType + " json:" + datapointJson);
            addDataPoint(iataCode, pointType, gson.fromJson(datapointJson, DataPoint.class));
        } catch (WeatherException e) {
            LOGGER.log(Level.SEVERE, null, e);
        }
        return Response.status(Response.Status.OK).build();
    }

    /**
     * get list of airports
     *
     * @return status
     */
    @GET
    @Path("/airports")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getAirports() {
        Set<String> retval = new HashSet<>();
        LOGGER.log(Level.FINE, "getAirports -> list.size:" + AirportRepository.list().size());
        for (AirportData ad : AirportRepository.list()) {
            retval.add(ad.getIata());
        }
        return Response.status(Response.Status.OK).entity(retval).build();
    }

    /**
     * get airport
     *
     * @param iata
     * @return status
     */
    @GET
    @Path("/airport/{iata}")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response getAirport(@PathParam("iata") String iata) {
        AirportData ad = findAirportData(iata);
        LOGGER.log(Level.FINE, "getAirport -> iata:" + ad.toString());
        return Response.status(Response.Status.OK).entity(ad).build();
    }

    /**
     * add airport
     *
     * @param iata
     * @param latString
     * @param longString
     * @return status
     */
    @POST
    @Path("/airport/{iata}/{lat}/{long}")
    @Override
    public Response addAirport(@PathParam("iata") String iata,
            @PathParam("lat") String latString,
            @PathParam("long") String longString) {
        LOGGER.log(Level.FINE, "addAirport -> iata:" + iata + " lat:" + latString + " long:" + longString);
        addAirport(iata, Double.valueOf(latString), Double.valueOf(longString));
        return Response.status(Response.Status.OK).build();
    }

    /**
     * delete airport
     *
     * @param iata
     * @return status
     */
    //TODO implement service
    @DELETE
    @Path("/airport/{iata}")
    @Override
    public Response deleteAirport(@PathParam("iata") String iata) {
        LOGGER.log(Level.FINE, "delete Airport -> iata:" + iata);
        boolean ok = deleteAirportFromList(iata);
        if (ok) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    /**
     * create new airport in the list
     *
     * @param jsonData
     * @return status
     */
    @POST
    @Path("/airport/new")
    @Override
    public Response addAirport(String jsonData) {
        LOGGER.log(Level.FINE, "create Airport -> json:" + jsonData);
        AirportData data = createNewAirport(jsonData);
        return Response.status(Response.Status.OK).entity(data).build();
    }

    //
    // Internal support methods
    //
    /**
     * Update the airports weather data with the collected data.
     *
     * @param iataCode the 3 letter IATA code
     * @param pointType the point type {@link DataPointType}
     * @param dp a datapoint object holding pointType data
     *
     * @throws WeatherException if the update can not be completed
     */
    public void addDataPoint(String iataCode, String pointType, DataPoint dp) throws WeatherException {
        LOGGER.log(Level.FINE, "addDataPoint -> iataCode:" + iataCode + " pointType:" + pointType + " DataPoint:" + dp.toString());
        int airportDataIdx = getAirportDataIdx(iataCode);
        AtmosphericInformation ai = new AtmosphericInformation();
        if (pointType.equals("wind")) {
            ai.setWind(dp);
        } else if (pointType.equals("temperature")) {
            ai.setTemperature(dp);
        } else if (pointType.equals("humidity")) {
            ai.setHumidity(dp);
        } else if (pointType.equals("pressipitation")) {
            ai.setPrecipitation(dp);
        } else if (pointType.equals("pressure")) {
            ai.setPressure(dp);
        } else {
            ai.setCloudCover(dp);
        }
        updateAtmosphericInformation(ai, pointType, dp);
    }

    /**
     * update atmospheric information with the given data point for the given
     * point type
     *
     * @param ai the atmospheric information object to update
     * @param pointType the data point type as a string
     * @param dp the actual data point
     * @throws com.crossover.trial.weather.exception.WeatherException
     */
    public void updateAtmosphericInformation(AtmosphericInformation ai, String pointType, DataPoint dp) throws WeatherException {

        LOGGER.log(Level.FINE, "addDataPoint -> AtmosphericInformation:" + ai.toString() + " pointType:" + pointType + " DataPoint:" + dp.toString());
        if (pointType.equalsIgnoreCase(DataPointType.WIND.getValue())) {
            if (dp.getMean() >= 0) {
                ai.setWind(dp);
                ai.setLastUpdateTime(System.currentTimeMillis());
                AirportRepository.add(ai);
                return;
            }
        }

        if (pointType.equalsIgnoreCase(DataPointType.TEMPERATURE.getValue())) {
            if (dp.getMean() >= -50 && dp.getMean() < 100) {
                ai.setTemperature(dp);
                ai.setLastUpdateTime(System.currentTimeMillis());
                AirportRepository.add(ai);
                return;
            }
        }

        if (pointType.equalsIgnoreCase(DataPointType.HUMIDITY.getValue())) {
            if (dp.getMean() >= 0 && dp.getMean() < 100) {
                ai.setHumidity(dp);
                ai.setLastUpdateTime(System.currentTimeMillis());
                AirportRepository.add(ai);
                return;
            }
        }

        if (pointType.equalsIgnoreCase(DataPointType.PRESSURE.getValue())) {
            if (dp.getMean() >= 650 && dp.getMean() < 800) {
                ai.setPressure(dp);
                ai.setLastUpdateTime(System.currentTimeMillis());
                AirportRepository.add(ai);
                return;
            }
        }

        if (pointType.equalsIgnoreCase(DataPointType.CLOUDCOVER.getValue())) {
            if (dp.getMean() >= 0 && dp.getMean() < 100) {
                ai.setCloudCover(dp);
                ai.setLastUpdateTime(System.currentTimeMillis());
                AirportRepository.add(ai);
                return;
            }
        }

        if (pointType.equalsIgnoreCase(DataPointType.PRECIPITATION.getValue())) {
            if (dp.getMean() >= 0 && dp.getMean() < 100) {
                ai.setPrecipitation(dp);
                ai.setLastUpdateTime(System.currentTimeMillis());
                AirportRepository.add(ai);
                return;
            }
        }

        throw new IllegalStateException("couldn't update atmospheric data");
    }

    /**
     * Add a new known airport to our list.
     *
     * @param iataCode 3 letter code
     * @param latitude in degrees
     * @param longitude in degrees
     *
     * @return the added airport
     */
    public static void addAirport(String iataCode, double latitude, double longitude) {
        LOGGER.log(Level.FINE, "addAirport -> iataCode:" + iataCode + " latitude:" + latitude + " longitude:" + longitude);
        AirportData ad = new AirportData();
        ad.setIata(iataCode);
        ad.setLatitude(latitude);
        ad.setLatitude(longitude);
        AirportRepository.add(ad);
        AtmosphericInformation ai = new AtmosphericInformation();
        AirportRepository.add(ai);
    }

    /**
     * init block
     */
    protected static void init() {
        AirportRepository.clear();
        requestFrequency.clear();
    }

    /**
     * create new airport
     *
     * @param json
     * @return AirportData
     */
    private AirportData createNewAirport(String json) {
        AirportData airportData = null;
        try {
            airportData = gson.fromJson(json, AirportData.class);
            AirportRepository.add(airportData);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "error on create new airport: " + json, e);
        }
        return airportData;
    }

    /**
     * delete airport
     *
     * @param iata
     * @return boolean
     */
    private boolean deleteAirportFromList(String iata) {
        boolean deleted = false;
        AirportData airportData = new AirportData();
        airportData.setIata(iata);
        try {
            airportData = AirportRepository.get(airportData);
            if (airportData != null) {
                AirportRepository.remove(airportData);
                deleted = true;
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "error on delete airport: " + iata, e);
        }
        return deleted;
    }

}
