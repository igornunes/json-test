// CR:restructure package
package com.crossover.trial.weather;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * A collected point, including some information about the range of collected values
 *
 * @author code test administrator
 */
// CR:implememt hashcode
public class DataPoint {

    // CR:remove initial value
    public double mean = 0.0;

    // CR:remove initial value
    public int first = 0;

    // CR:remove initial value
    public int second = 0;

    // CR:remove initial value
    public int third = 0;

    // CR:remove initial value
    public int count = 0;

    /** private constructor, use the builder to create this object */
    private DataPoint() { }

    // CR:javadoc
    protected DataPoint(int first, int second, int mean, int third, int count) {
        this.setFirst(first);
        this.setMean(mean);
        this.setSecond(second);
        this.setThird(third);
        this.setCount(count);
    }

    /** the mean of the observations */
    // CR:use this
    public double getMean() {
        return mean;
    }

    // CR:javadoc
    protected void setMean(double mean) { this.mean = mean; }

    /** 1st quartile -- useful as a lower bound */
    // CR:use this
    public int getFirst() {
        return first;
    }

    // CR:javadoc
    protected void setFirst(int first) {
        this.first = first;
    }

    /** 2nd quartile -- median value */
    // CR:use this
    public int getSecond() {
        return second;
    }

    // CR:javadoc
    protected void setSecond(int second) {
        this.second = second;
    }

    /** 3rd quartile value -- less noisy upper value */
    // CR:use this
    public int getThird() {
        return third;
    }

    // CR:javadoc
    protected void setThird(int third) {
        this.third = third;
    }

    /** the total number of measurements */
    // CR:use this
    public int getCount() {
        return count;
    }

    // CR:javadoc
    protected void setCount(int count) {
        this.count = count;
    }

    // CR:javadoc
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

    // CR:rewrite using atributes
    public boolean equals(Object that) {
        return this.toString().equals(that.toString());
    }

    // CR:non sense - remove entire inner class
    static public class Builder {
        int first;
        int mean;
        int median;
        int last;
        int count;

        public Builder() { }

        public Builder withFirst(int first) {
            first= first;
            return this;
        }

        public Builder withMean(int mean) {
            mean = mean;
            return this;
        }

        public Builder withMedian(int median) {
            median = median;
            return this;
        }

        public Builder withCount(int count) {

            count = count;
            return this;
        }

        public Builder withLast(int last) {
            last = last;
            return this;
        }

        public DataPoint build() {
            return new DataPoint(this.first, this.mean, this.median, this.last, this.count);
        }
    }
}
