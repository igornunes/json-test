package com.crossover.trial.weather.model;

import java.util.Objects;

/**
 * encapsulates sensor information for a particular location
 */
public class AtmosphericInformation {

    /**
     * temperature in degrees celsius
     */
    private DataPoint temperature;

    /**
     * wind speed in km/h
     */
    private DataPoint wind;

    /**
     * humidity in percent
     */
    private DataPoint humidity;

    /**
     * precipitation in cm
     */
    private DataPoint precipitation;

    /**
     * pressure in mmHg
     */
    private DataPoint pressure;

    /**
     * cloud cover percent from 0 - 100 (integer)
     */
    private DataPoint cloudCover;

    /**
     * the last time this data was updated, in milliseconds since UTC epoch
     */
    private long lastUpdateTime;

    /**
     * constructor
     */
    public AtmosphericInformation() {
    }

    /**
     * constructor
     *
     * @param temperature
     * @param wind
     * @param humidity
     * @param percipitation
     * @param pressure
     * @param cloudCover
     */
    public AtmosphericInformation(DataPoint temperature, DataPoint wind, DataPoint humidity, DataPoint percipitation, DataPoint pressure, DataPoint cloudCover) {
        this.temperature = temperature;
        this.wind = wind;
        this.humidity = humidity;
        this.precipitation = percipitation;
        this.pressure = pressure;
        this.cloudCover = cloudCover;
        this.lastUpdateTime = System.currentTimeMillis();
    }

    /**
     * getter temperature
     *
     * @return DataPoint
     */
    public DataPoint getTemperature() {
        return this.temperature;
    }

    /**
     * setter temperature
     *
     * @param temperature
     */
    public void setTemperature(DataPoint temperature) {
        this.temperature = temperature;
    }

    /**
     * getter wind
     *
     * @return wind
     */
    public DataPoint getWind() {
        return this.wind;
    }

    /**
     * setter wind
     *
     * @param wind
     */
    public void setWind(DataPoint wind) {
        this.wind = wind;
    }

    /**
     * getter humidity
     *
     * @return humidity
     */
    public DataPoint getHumidity() {
        return this.humidity;
    }

    /**
     * setter humidity
     *
     * @param humidity
     */
    public void setHumidity(DataPoint humidity) {
        this.humidity = humidity;
    }

    /**
     * getter precipitation
     *
     * @return precipitation
     */
    public DataPoint getPrecipitation() {
        return this.precipitation;
    }

    /**
     * setter precipitation
     *
     * @param precipitation
     */
    public void setPrecipitation(DataPoint precipitation) {
        this.precipitation = precipitation;
    }

    /**
     * getter pressure
     *
     * @return pressure
     */
    public DataPoint getPressure() {
        return this.pressure;
    }

    /**
     * setter pressure
     *
     * @param pressure
     */
    public void setPressure(DataPoint pressure) {
        this.pressure = pressure;
    }

    /**
     * getter cloudCover
     *
     * @return cloudCover
     */
    public DataPoint getCloudCover() {
        return this.cloudCover;
    }

    /**
     * setter cloudCover
     *
     * @param cloudCover
     */
    public void setCloudCover(DataPoint cloudCover) {
        this.cloudCover = cloudCover;
    }

    /**
     * getter lastUpdateTime
     *
     * @return lastUpdateTime
     */
    public long getLastUpdateTime() {
        return this.lastUpdateTime;
    }

    /**
     * setter lastUpdateTime
     *
     * @param lastUpdateTime
     */
    public void setLastUpdateTime(long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    /**
     * hashcode
     *
     * @return integer
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.temperature);
        hash = 73 * hash + Objects.hashCode(this.wind);
        hash = 73 * hash + Objects.hashCode(this.humidity);
        hash = 73 * hash + Objects.hashCode(this.precipitation);
        hash = 73 * hash + Objects.hashCode(this.pressure);
        hash = 73 * hash + Objects.hashCode(this.cloudCover);
        return hash;
    }

    /**
     * equals
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AtmosphericInformation other = (AtmosphericInformation) obj;
        if (!Objects.equals(this.temperature, other.temperature)) {
            return false;
        }
        if (!Objects.equals(this.wind, other.wind)) {
            return false;
        }
        if (!Objects.equals(this.humidity, other.humidity)) {
            return false;
        }
        if (!Objects.equals(this.precipitation, other.precipitation)) {
            return false;
        }
        if (!Objects.equals(this.pressure, other.pressure)) {
            return false;
        }
        if (!Objects.equals(this.cloudCover, other.cloudCover)) {
            return false;
        }
        return true;
    }

}
