// CR:restructure package
package com.crossover.trial.weather;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 * A reference implementation for the weather client. Consumers of the REST API can look at WeatherClient
 * to understand API semantics. This existing client populates the REST endpoint with dummy data useful for
 * testing.
 *
 * @author code test administrator
 */
public class WeatherClient {

    // CR:javadoc, get value from properties
    private static final String BASE_URI = "http://localhost:8080";
    /** end point for read queries */
    private WebTarget query;

    /** end point to supply updates */
    private WebTarget collect;

    // CR:javadoc, 
    public WeatherClient() {
        Client client = ClientBuilder.newClient();
        query = client.target(BASE_URI + "/query");
        // CR:use base_uri
        collect = client.target("http://localhost:8080/collect");
    }

    // CR:javadoc
    public void pingCollect() {
        WebTarget path = collect.path("/ping");
        Response response = path.request().get();
        System.out.print("collect.ping: " + response.readEntity(String.class) + "\n");
    }

    // CR:javadoc
    public void pingQuery() {
        WebTarget path = query.path("/ping");
        Response response = path.request().get();
        System.out.println("query.ping: " + response.readEntity(String.class));
    }

    // CR:javadoc
    public void populate() {
        WebTarget path = collect.path("/weather/BOS/wind");
        DataPoint dp = new DataPoint.Builder()
                .withFirst(0).withLast(10).withMean(4).withMedian(4).withCount(10)
                .build();
        // CR:not user variable - remove
        Response post = path.request().post(Entity.entity(dp, "application/json"));
    }

    // CR:javadoc
    public void query() {
        WebTarget path = query.path("/weather/BOS/0");
        Response response = path.request().get();
        System.out.println("query.get:" + response.readEntity(String.class));
    }

    // CR:javadoc
    public static void main(String[] args) {
        WeatherClient wc = new WeatherClient();
        wc.pingCollect();
        wc.populate();
        wc.query();
        wc.pingQuery();
        System.out.print("complete");
        System.exit(0);
    }
}
