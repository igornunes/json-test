package com.crossover.trial.weather.client;

import com.crossover.trial.weather.model.DataPoint;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 * A reference implementation for the weather client. Consumers of the REST API
 * can look at WeatherClient to understand API semantics. This existing client
 * populates the REST endpoint with dummy data useful for testing.
 *
 * @author code test administrator
 */
public class WeatherClient {

    public final static Logger LOGGER = Logger.getLogger(WeatherClient.class.getName());

    // CR:javadoc, get value from properties
    private static String BASE_URL;
    /**
     * end point for read queries
     */
    private WebTarget query;

    /**
     * end point to supply updates
     */
    private WebTarget collect;

    /**
     * constructor
     */
    public WeatherClient() {
        try {
            Client client = ClientBuilder.newClient();
            Properties prop = new Properties();
            prop.load(WeatherClient.class.getClassLoader().getResourceAsStream("application.properties"));
            BASE_URL = prop.getProperty("baseUrl");
            query = client.target(BASE_URL + "query");
            collect = client.target(BASE_URL + "collect");
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "error on create client", e);
        }
    }

    /**
     * ping method
     */
    public void pingCollect() {
        WebTarget path = collect.path("/ping");
        Response response = path.request().get();
        LOGGER.log(Level.INFO, "collect.ping: " + response.readEntity(String.class));
    }

    /**
     * ping method
     */
    public void pingQuery() {
        WebTarget path = query.path("/ping");
        Response response = path.request().get();
        LOGGER.log(Level.INFO, "query.ping: " + response.readEntity(String.class));
    }

    /**
     * populate method
     */
    public void populate() {
        LOGGER.log(Level.FINE, "populate method");
        WebTarget path = collect.path("/weather/BOS/wind");
        DataPoint dp = new DataPoint(0, 4, 4, 10, 10);
        path.request().post(Entity.entity(dp, "application/json"));
    }

    /**
     * exec query method
     */
    public void query() {
        LOGGER.log(Level.FINE, "query method");
        WebTarget path = query.path("/weather/BOS/0");
        Response response = path.request().get();
        System.out.println("query.get:" + response.readEntity(String.class));
    }

    /**
     * main method used to test service
     *
     * @param args
     */
    public static void main(String[] args) {
        LOGGER.log(Level.FINE, "Main method");
        WeatherClient wc = new WeatherClient();
        wc.pingCollect();
        wc.populate();
        wc.query();
        wc.pingQuery();
        LOGGER.log(Level.INFO, "complete");
        System.exit(0);
    }
}
