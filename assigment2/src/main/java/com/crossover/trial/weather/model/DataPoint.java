package com.crossover.trial.weather.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * A collected point, including some information about the range of collected
 * values
 *
 * @author code test administrator
 */
public class DataPoint {

    /**
     * mean
     */
    private double mean;

    /**
     * first
     */
    private int first;

    /**
     * median
     */
    private int median;

    /**
     * third
     */
    private int third;

    /**
     * count
     */
    private int count;

    /**
     * constructor
     */
    public DataPoint() {
        this.first = 0;
        this.mean = 0.0;
        this.median = 0;
        this.third = 0;
        this.count = 0;
    }

    /**
     * constructor
     *
     * @param first
     * @param mean
     * @param median
     * @param third
     * @param count
     */
    public DataPoint(int first, int mean, int median, int third, int count) {
        this.first = first;
        this.mean = mean;
        this.median = median;
        this.third = third;
        this.count = count;
    }

    /**
     * the mean of the observations
     *
     * @return double
     */
    public double getMean() {
        return this.mean;
    }

    /**
     * setter mean
     *
     * @param mean
     */
    public void setMean(double mean) {
        this.mean = mean;
    }

    /**
     * 1st quartile -- useful as a lower bound
     *
     * @return integer
     */
    public int getFirst() {
        return this.first;
    }

    /**
     * setter first
     *
     * @param first
     */
    public void setFirst(int first) {
        this.first = first;
    }

    /**
     * 2nd quartile -- median value
     *
     * @return integer
     */
    public int getMedian() {
        return this.median;
    }

    /**
     * setter median
     *
     * @param median
     */
    public void setMedian(int median) {
        this.median = median;
    }

    /**
     * 3rd quartile value -- less noisy upper value
     *
     * @return integer
     */
    public int getThird() {
        return this.third;
    }

    /**
     * setter third
     *
     * @param third
     */
    public void setThird(int third) {
        this.third = third;
    }

    /**
     * the total number of measurements
     *
     * @return integer
     */
    public int getCount() {
        return this.count;
    }

    /**
     * setter count
     *
     * @param count
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * toString
     *
     * @return
     */
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

    /**
     * equals
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DataPoint other = (DataPoint) obj;
        if (Double.doubleToLongBits(this.mean) != Double.doubleToLongBits(other.mean)) {
            return false;
        }
        if (this.first != other.first) {
            return false;
        }
        if (this.median != other.median) {
            return false;
        }
        if (this.third != other.third) {
            return false;
        }
        if (this.count != other.count) {
            return false;
        }
        return true;
    }

    /**
     * hashcode
     *
     * @return integer
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.mean) ^ (Double.doubleToLongBits(this.mean) >>> 32));
        hash = 17 * hash + this.first;
        hash = 17 * hash + this.median;
        hash = 17 * hash + this.third;
        hash = 17 * hash + this.count;
        return hash;
    }

}
