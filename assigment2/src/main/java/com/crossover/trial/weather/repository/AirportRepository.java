package com.crossover.trial.weather.repository;

import com.crossover.trial.weather.model.AirportData;
import com.crossover.trial.weather.model.AtmosphericInformation;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.jetty.util.ConcurrentHashSet;

/**
 * repository data for airportData
 *
 * @author igor
 */
public class AirportRepository {

    /**
     * separator
     */
    private static String SEPARATOR = ",";

    public final static Logger LOGGER = Logger.getLogger(AirportRepository.class.getName());

    /**
     * initialize repository
     */
    static {
        load();
    }

    /**
     * used to manipulate data in memory of AirportData
     */
    private static Set<AirportData> airports;
    
    /**
     * used to manipulate data in memory of AirportData
     */
    private static Set<AtmosphericInformation> atmosphericInformations;

    /**
     * load data from file
     */
    private static void load() {
        airports = new ConcurrentHashSet<>();
        atmosphericInformations = new ConcurrentHashSet<>();
        Properties prop = new Properties();
        try {
            prop.load(AirportRepository.class.getClassLoader().getResourceAsStream("application.properties"));
            String dataFile = prop.getProperty("dataFile");
            BufferedReader reader = new BufferedReader(new InputStreamReader(AirportRepository.class.getClassLoader().getResourceAsStream(dataFile)));
            String line = "";
            while ((line = reader.readLine()) != null) {
                if (line != null && !line.equals("")) {
                    String[] aux = line.split(SEPARATOR);
                    airports.add(new AirportData(Integer.parseInt(aux[0]),
                            aux[1],aux[2] + " - "+ aux[3],aux[4],aux[5],Double.parseDouble(aux[6]),
                            Double.parseDouble(aux[7]),Double.parseDouble(aux[8]),Double.parseDouble(aux[9]),
                            aux[10]));
                }
            }
            reader.close();
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }

    /**
     * remove object
     *
     * @param data
     * @return boolean
     */
    public static boolean remove(AirportData data) {
        boolean ok = false;
        if (airports.remove(data)) {
            ok = true;
        }
        return ok;
    }
    
    /**
     * remove object
     *
     * @param data
     * @return boolean
     */
    public static boolean remove(AtmosphericInformation data) {
        boolean ok = false;
        if (atmosphericInformations.remove(data)) {
            ok = true;
        }
        return ok;
    }

    /**
     * add object
     *
     * @param data
     * @return boolean
     */
    public static boolean add(AirportData data) {
        boolean ok = false;
        if (airports.contains(data)) {
            airports.remove(data);
        }
        if (airports.add(data)) {
            ok = true;
        }
        return ok;
    }
    
    /**
     * add object
     *
     * @param data
     * @return boolean
     */
    public static boolean add(AtmosphericInformation data) {
        boolean ok = false;
        if (atmosphericInformations.contains(data)) {
            atmosphericInformations.remove(data);
        }
        if (atmosphericInformations.add(data)) {
            ok = true;
        }
        return ok;
    }

    /**
     * get object
     *
     * @param data
     * @return AirportData
     */
    public static AirportData get(AirportData data) {
        List<AirportData> list = new ArrayList<>();
        list.addAll(airports);
        return list.get(list.indexOf(data));
    }
    
    /**
     * get object
     *
     * @param data
     * @return AtmosphericInformation
     */
    public static AtmosphericInformation get(AtmosphericInformation data) {
        List<AtmosphericInformation> list = new ArrayList<>();
        list.addAll(atmosphericInformations);
        return list.get(list.indexOf(data));
    }

    /**
     * get object
     *
     * @param index
     * @return AirportData
     */
    public static AirportData get(int index) {
        List<AirportData> list = new ArrayList<>();
        list.addAll(airports);
        Collections.sort(list);
        return list.get(index);
    }
    
    /**
     * get object
     *
     * @param index
     * @return AtmosphericInformation
     */
    public static AtmosphericInformation getAtmInfo(int index) {
        List<AtmosphericInformation> list = new ArrayList<>();
        list.addAll(atmosphericInformations);
        return list.get(index);
    }

    /**
     * get list of AirportData
     *
     * @return List<AirportData>
     */
    public static List<AirportData> list() {
        List<AirportData> list = new ArrayList<>();
        list.addAll(airports);
        Collections.sort(list);
        return list;
    }
    
    /**
     * get list of AtmosphericInformation
     *
     * @return List<AtmosphericInformation>
     */
    public static List<AtmosphericInformation> listAtmInfo() {
        List<AtmosphericInformation> list = new ArrayList<>();
        list.addAll(atmosphericInformations);
        return list;
    }

    /**
     * get list of AirportData
     *
     * @return List<AirportData>
     */
    public static List<AirportData> list(AirportData... filter) {
        List<AirportData> list = new ArrayList<>();
        list.addAll(airports);
        list.retainAll(Arrays.asList(filter));
        Collections.sort(list);
        return list;
    }

    /**
     * write data in file before server shutdown
     */
    public static void write() {
        Properties prop = new Properties();
        try {
            prop.load(AirportRepository.class.getClassLoader().getResourceAsStream("application.properties"));
            String dataFile = prop.getProperty("dataFile");
            File file = new File(AirportRepository.class.getClassLoader().getResource(dataFile).getPath());
            FileWriter fileWriter = new FileWriter(file, false);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            for (AirportData airport : airports) {
                writer.write(airport.toString() + "\r\n");
            }
            writer.flush();
            writer.close();
            fileWriter.close();
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

    }

    /**
     * clear atmosphericInformation list
     */
    public static void clear() {
        atmosphericInformations = new ConcurrentHashSet<>();
    }

}
